<?php
    include "modelo/datos.php";
    session_start();
    $usuario =$_SESSION['usuario'];  
    if(!$usuario){
        header('location:login.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventario</title>
    <link rel="stylesheet" href="css/estilos.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
</head>
<body>
    
<div class="container">    <!-- para centrar el login y justificarlo -->
                 <div class="table-responsive"><!-- para ahcer tabla responsiva -->
                    <h4 class="display-4 text-center">Datos de Contacto</h4><hr>
                    <h4>Bienvenido:<?php echo $usuario;?></h4>
                   
                     <div class="link-right"> 
                            
                            <a href="modelo/salir.php" class="btn btn-primary">Salir</a>
                        </div><br>
                    <?php if(mysqli_num_rows($result)){?>  
                                          
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>  
                                <th scope="col">correo</th>
                                <th scope="col">asunto</th>
                                <th scope="col">mensaje</th>   
                                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 0;
                                    while($rows=mysqli_fetch_assoc($result)){       
                                        $i++;                                     
                                ?>
                                <tr>
                                <th scope="row"><?=$i?></th>
                                <td><?=$rows['nombre']?></td>
                                <td><?=$rows['correo']?></td>
                                <td><?=$rows['asunto']?></td>
                                <td><?=$rows['mensaje']?></td>
                                <td><a href="modelo/delete.php?id=<?=$rows['id']?>"
                                 class="btn btn-danger">Eliminar</a>                                
                                </td>
                               
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php } ?>
                       
                </div>  
    </div>    
</body>
</html>