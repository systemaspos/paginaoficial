<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SystemasPos</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Day - v4.7.0
  * Template URL: https://bootstrapmade.com/day-multipurpose-html-template-for-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope-fill"></i><a href="mailto:contact@example.com">systemaspos@gmail.com</a>
        <i class="bi bi-phone-fill phone-icon"></i> +57 3133485269
      </div>
      <div class="social-links d-none d-md-block">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a href="index.html">SystemasPos</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Principal</a></li>
          <li><a class="nav-link scrollto" href="#about">Nosotros</a></li>
          <li><a class="nav-link scrollto" href="#services">Servicios</a></li>
          <li><a class="nav-link scrollto " href="#portfolio">Portafolio</a></li>
          <li><a class="nav-link scrollto" href="#pricing">Precios</a></li>
          <li><a class="nav-link scrollto" href="#team">Equipo</a></li>
          <li class="dropdown"><a href="#"><span>Mas</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="login/login.php">Ingreso</a></li>
              <li class="dropdown"><a href="#"><span>Mas</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                </ul>
              </li>
           
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contactanos</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">
    <div class="container position-relative" data-aos="fade-up" data-aos-delay="500">
      <h1>SystemasPos.com</h1>
      <h2>Soluciones Tecnologicas</h2>
      <a href="#about" class="btn-get-started scrollto">Empezar Recorrido</a>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="row">
          <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
	    <img src="assets/img/diagrama.jpg" class="img-fluid" alt="">
          </div>
	 
          <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right">
            <h3>Sobre nosotros.</h3>
            <p class="fst-italic">
              Systemaspos  nace en el 2021 como un emprendimiento de 3 ingenieros "Carlos Arnulfo Cogua Laverde, Diego Libardo Melo Maecha, Osmar Yesid Rincon Zorro" egresados de la universidad Remington y fundado en Abril del 2022  en Yopal Casanare.
            </p>
            <ul>
              <li><i class="bi bi-check-circle"></i> Con la Principal intención de crear soluciones tecnológicas que beneficien al libre desarrollo y evolución de la economía en los emprendedores y pequeñas empresas.</li>
              <li><i class="bi bi-check-circle"></i> Brindar un excelente servicio de administración, análisis, marketing digital, soporte y asesoría tecnológica online.
</li>
              <li><i class="bi bi-check-circle"></i> Resultados en Tiempo de respuesta cortos.</li>
            </ul>
	<p>Systemaspos cuenta actualmente con una herramienta de punto de ventas online llamado Admin-Fa el cual funciona solo en la web, nuestro propósito es enseñarle a los emprendedores o pequeñas empresas, que podemos hacer crecer exponencialmente sus ventas y digitalizar de manera sostenible su administración para ampliar la vida de sus empresas, que sean generadoras de empleo y que juntos testifiquemos que crear empresa en Colombia ¡¡si paga!!.


</p>
	<br>
	<p>Queremos que la sociedad despierte y evolucione, si no cuentan con un sistema de administración, con nosotros aprenderán a usarlo. Y si ya poseen un sistema de administración local, deseamos que miren más halla de las posibilidades y no permitan que los datos de sus ventas y servicios se conviertan en bases de datos abandonadas en un Pc, que dejen lo local y vengas con nosotros a esta aventura por la web.


</p>
<br>
	<p>Una página web informativa es importante, pero una plataforma dinámica para administrar tus ventas en establecimientos físicos y de manera virtual esta de moda.


 </p>
</p>
<br>
	<p>Nuestro producto es capaz de monitorear en tiempo real las ventas de su establecimiento, mostrar gráficas de eficiencia, administrar los diferentes roles, para que usted como nuestro cliente y principal prioridad, se sienta confiado en la administración online de las finanzas de su emprendimiento o empresa.


 </p>
</p>
<br>
	<p>Nuestros servicios son completamente online, usted contará con un Dominio con el nombre de su empresa www.nobredetuempresa.com, una página principal corporativa con la información empresarial, login para acceder al sistema de ventas Admin-Fa, galería de productos y servicios, formulario de contacto.


 </p>
           
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">

          <div class="col-lg-4" data-aos="fade-up">
            <div class="box">
              <span>M</span>
              <h4>Misión</h4>
              <p>Nuestra misión es brindar soluciones tecnológicas a todo el sector comercial, emprendedores y PYMES, entregando confiabilidad y confort con el uso de nuestras herramientas web, Prestando servicios online de manera eficaz para satisfacer todas las necesidades de nuestros clientes y garantizando la mejora continua en la sistematización total del proceso de ventas, promoción, imagen, marketing, entro otros servicios.


</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="150">
            <div class="box">
              <span>V</span>
              <h4>Visión</h4>
              <p>Nuestra visión para el año 2027 es ser reconocidos como una de las empresas más nombradas que proveen de tecnología web en el departamento de Casanare y en el territorio de Colombia, Ofreciendo servicios adicionales como Analítica de datos comerciales, Marketing digital, Inteligencia Artificial, Seguridad Informática, Proveedores de servicios de sistemas cerrados de vigilancia, imagen corporativa y diseño gráfico, Auditoria y aseaorias organizacionales.


</p>
            </div>
          </div>
 <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="box">
              <span>P</span>
              <h4> Política de No conformismo</h4>
              <p>En nuestro equipo de trabajo velamos porque la motivación sea constante y nuestras ganas de emprender infinitas, no aceptamos la conformidad en nuestros proyectos y tampoco queremos que nuestros usuarios se conformen con sus cifras,  que sus clientes y ventas aumenten y pasen de ser emprendedores o pequeñas empresas a empresas con más de 10 años en el mercado, deseamos que siempre esperen lo mejor de nosotros, porque el mejoramiento continuo es uno de nuestras más grandes retos.


</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
            <div class="box">
              <span>V</span>
              <h4> Valores</h4>
              <p>Systemaspos está centrado en el respeto, la honestidad, el amor propio y la pasión por ayudar a la sociedad y la evolución económica de las familias que estén emprendiendo, en la solución de problemas cotidianos y en la búsqueda de la perfección para lograr el mayor logro y es la satisfacción de nuestros clientes.


</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients">
	<p>Clientes</p>
      <div class="container" data-aos="zoom-in">

        <div class="row d-flex align-items-center">

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-1.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-2.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-3.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-4.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-5.png" class="img-fluid" alt="">
          </div>

          <div class="col-lg-2 col-md-4 col-6">
            <img src="assets/img/clients/client-6.png" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Clients Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
        <div class="container">

            <div class="section-title">
                <span>Servicios</span>
                    <h2>Servicios</h2>
                <p>Puedes observar todos los revicios que tenemos para tu empresa</p>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bxl-dribbble"></i></div>
                                <h4><a href="">Diseño de Paginas Vew Informativas</a></h4>
                                <p>Página web de diseño, con navegador vanguardista, pestañas de historia empresarial, galería dinámica de imágenes, portafolio de servicios, formulario de contacto
                                    </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="150">
                    <div class="icon-box">
                            <div class="icon"><i class="bx bx-file"></i></div>
                        <h4><a href="">Sitios webs Administrables</a></h4>
                        <p>Los mismos componentes de la pagina web informativa + sistema de ventas Admin-Fa</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-tachometer"></i></div>
                        <h4><a href="">Asesorias Corporativas</a></h4>
                        <p>Planeacion de proyectos, revision de sistema organizacional empresarial, asesoramiento corporativo</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="450">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-world"></i></div>
                        <h4><a href="">Imagen Corporativa</a></h4>
                        <p>Estudio fotografico, diseño grafico y mejoramiento de su imagen empresarial</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="600">
                    <div class="icon-box">
                        <div class="icon"><i class="bx bx-slideshow"></i></div>
                        <h4><a href="">Analisìs de datos empresariales</a></h4>
                        <p>Analisamos con su consentimiento, las bases de datos de su software o de nuestras herramientas para entregar informes estratégicos de ventas y adquisición de productos, disminuyendo sus egresos y promoviendo el incremento de sus ingresos, generando estrategias comerciales para que su empresa sea lider en el mercado y aumente exponencialmente el numero de sus clientes
                        </p>
                </div>
            </div>
        </div>  
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
        <div class="container" data-aos="zoom-in">

            <div class="text-center">
                <h3>Puedes Aumentar Tus finanzas, confia en nosotros por que solo existimos para servir a la comunidad</h3>
                <p> Dale a tu emprendimiento el impulso correcto para subir al siguiente nivel, Estamos en la era tecnologica y puedes quedarte atras.</p>
                <a class="cta-btn" href="#">Click para subir al Inicio</a>
            </div>

        </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section 
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <span>Portfolio</span>
          <h2>Portfolio</h2>
          <p>Sit sint consectetur velit quisquam cupiditate impedit suscipit alias</p>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="150">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 1</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-2.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 2</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 2</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 2</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-5.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>App 3</h4>
              <p>App</p>
              <a href="assets/img/portfolio/portfolio-6.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 1</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-7.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <img src="assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Card 3</h4>
              <p>Card</p>
              <a href="assets/img/portfolio/portfolio-8.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web">
            <img src="assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Web 3</h4>
              <p>Web</p>
              <a href="assets/img/portfolio/portfolio-9.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

        </div>

      </div>
    </section> ======= -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
        <div class="container">

            <div class="section-title">
                <span>Precios</span>
                <h2>Precios</h2>
                <p>Adquiere uno de nuestros planes trimestrales, mensuales o anueales con buenos descuentos</p>
            </div>

            <div class="row">

                <div class="col-lg-4 col-md-6" data-aos="zoom-in" data-aos-delay="150">
                    <div class="box">
                    <h3>TRIMESTRE</h3>
                    <h4><sup>$</sup>270.000<span> / month</span></h4>
                    <ul>
                        <li>XXXXX</li>
                        <li>XXXXXXXXXXXX</li>
                        <li>XXXXXXXXXXXXXXXXX</li>
                        <li XXXXXXXXXX</li>
                        <li class="na">Antes 300.000</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Pedir Ahora</a>
                    </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mt-4 mt-md-0" data-aos="zoom-in">
                    <div class="box featured">
                        <h3>Servicio Semestral</h3>
                        <h4><sup>$</sup>510.000<span> / month</span></h4>
                        <ul>
                            <li>xxxxxxxxxxxx</li>
                            <li>xxxxxxxxxxxxxxxxx</li>
                            <li>xxxxxxxxxxxxxxxxxxxxxx</li>
                            <li>xxxxxxxxxxxxxxxxxxxxxxxxxxxx</li>
                            <li xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx</li>
                        </ul>
                        <div class="btn-wrap">
                                <a href="#" class="btn-buy">Pedir Ahora</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="150">
                    <div class="box">
                    <h3>Servicio Anual</h3>
                    <h4><sup>$</sup>1.000.000<span> / month</span></h4>
                    <ul>
                        <li>xxxxxxxxxx</li>
                        <li>xxxxxxxxxxxxxxx</li>
                        <li>xxxxxxxxxxxxxxxxxxxx</li>
                        <li>xxxxxxxxxxxxxxxxxxxxxxxxxx</li>
                        <li>xxxxxxxxxxxxxxxxxxxxxxxxxxxx</li>
                    </ul>
                    <div class="btn-wrap">
                        <a href="#" class="btn-buy">Pedir Ahora</a>
                    </div>
                    </div>
                </div>                
            </div>
        </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
      <div class="container">

        <div class="section-title">
          <span>Team</span>
          <h2>Equipo</h2>
          <p>Equipo SYSTEMASPOS</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
            <div class="member">
              <img src="assets/img/team/team-1.jpg" alt="">
              <h4>Ing De Sistemas Carlos Arnulfo Cogua</h4>
              <span>Desarrollador, Lider de proyecto</span>
              <p>
                Joven de 30 años Yopaleño, Egresado de Uniremington
              </p>
              <div class="social">
                
                <a href="https://www.facebook.com/charlee.ees.35"><i class="bi bi-facebook"></i></a>
                
              </div>
            </div>
          </div>
	

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
            <div class="member">
              <img src="assets/img/team/team-2.jpg" alt="">
              <h4>Ing De Sistemas Yesid Rincon</h4>
              <span>Representante legal, Desarrollador web</span>
              <p>
                Joven de 23 años Boyacence, Egresado de Uniremington
              </p>
              <div class="social">
                
                <a href="https://www.facebook.com/yesidrincon.z"><i class="bi bi-facebook"></i></a>
                
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in">
            <div class="member">
              <img src="assets/img/team/team-3.jpg" alt="">
              <h4>Ing De Sistemas Diego Melo</h4>
              <span>Desarrollador web , Analista del sistema  </span>
              <p>
                Joven de 24 años Yopaleño, Egresado de Uniremington
              </p>
              <div class="social">
                
                <a href="https://www.facebook.com/diego.melomahecha"><i class="bi bi-facebook"></i></a>
                
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <span>Contacto</span>
          <h2>Contact</h2>
          <p>Con gusto puedes contactarno estamos agradecidos de poder ayudarte</p>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-lg-6">
            <div class="info-box mb-4">
              <i class="bx bx-map"></i>
              <h3>Nuestra direccion</h3>
              <p>Calle 33 transversal 5 41 - Trabajo remoto</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-envelope"></i>
              <h3>Correo electronico</h3>
              <p>Systemaspos@gmail.com</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <i class="bx bx-phone-call"></i>
              <h3>Telefonos</h3>
		<p>+573123902469</p>
	      <p>57+3133485269</p>
	      <p>+573209751481</p>
              
            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up">

          

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" >
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="nombre" class="form-control" id="name" placeholder="Nombres" value="<?php if(isset($_GET['nombre']))echo ($_GET['nombre']);?>"required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="correo" id="email" placeholder="Correo" value="<?php if(isset($_GET['correo']))echo ($_GET['correo']);?>" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="asunto" id="subject" placeholder="Asunto" value="<?php if(isset($_GET['asunto']))echo ($_GET['asunto']);?>" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="mensaje" rows="5" placeholder="Mensaje" value="<?php if(isset($_GET['mensaje']))echo ($_GET['mensaje']);?>" required></textarea>
              </div>
              <div class="my-3">
           
              </div>
              <div class="text-center"><button type="submit" name="enviar">Enviar</button></div>              
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6">
            <div class="footer-info">
              <h3>SystemasPos.com</h3>
              <p>
                A108 Adam Street <br>
                NY 535022, USA<br><br>
                <strong>Phone:</strong> +57 3133485269<br>
                <strong>Email:</strong> systemaspos@gmail.com<br>
              </p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Principal</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Nosotros</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Servicios</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>

          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>:)</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/day-multipurpose-html-template-for-free/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>